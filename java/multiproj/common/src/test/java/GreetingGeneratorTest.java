import org.junit.Test;

import static org.junit.Assert.*;

public class GreetingGeneratorTest {

    @Test
    public void testDefaultConstructor() {
        GreetingGenerator greetingGenerator = new GreetingGenerator();
        assertEquals("english", greetingGenerator.getLanguage());
    }

    @Test
    public void testParameterizedConstructor() {
        GreetingGenerator greetingGenerator = new GreetingGenerator("");
        assertEquals("english", greetingGenerator.getLanguage());

        greetingGenerator = new GreetingGenerator("english");
        assertEquals("english", greetingGenerator.getLanguage());

        greetingGenerator = new GreetingGenerator("Hawaiian");
        assertEquals("hawaiian", greetingGenerator.getLanguage());

        greetingGenerator = new GreetingGenerator(" english ");
        assertEquals("english", greetingGenerator.getLanguage());

        greetingGenerator = new GreetingGenerator(" ENGLISH ");
        assertEquals("english", greetingGenerator.getLanguage());

        greetingGenerator = new GreetingGenerator(" pig latin ");
        assertEquals("english", greetingGenerator.getLanguage());
    }

    @Test
    public void testGetGreetingEnglish() {
        GreetingGenerator greetingGenerator = new GreetingGenerator();

        assertEquals("Hello Sue", greetingGenerator.GetGreeting("Sue"));

        assertEquals("Hello ", greetingGenerator.GetGreeting(null));

        assertEquals("Hello ", greetingGenerator.GetGreeting(""));
    }


    @Test
    public void testGetGreetingHawaiian() {
        GreetingGenerator greetingGenerator = new GreetingGenerator("hawaiian");

        assertEquals("Aloha Sue", greetingGenerator.GetGreeting("Sue"));

        assertEquals("Aloha ", greetingGenerator.GetGreeting(null));

        assertEquals("Aloha ", greetingGenerator.GetGreeting(""));
    }

    @Test
    public void testGetGreetingSpanish() {
        GreetingGenerator greetingGenerator = new GreetingGenerator("spanish");

        assertEquals("Hola Sue", greetingGenerator.GetGreeting("Sue"));

        assertEquals("Hola ", greetingGenerator.GetGreeting(null));

        assertEquals("Hola ", greetingGenerator.GetGreeting(""));
    }

    @Test
    public void testGetGreetingGerman() {
        GreetingGenerator greetingGenerator = new GreetingGenerator("german");

        assertEquals("Hallo Sue", greetingGenerator.GetGreeting("Sue"));

        assertEquals("Hallo ", greetingGenerator.GetGreeting(null));

        assertEquals("Hallo ", greetingGenerator.GetGreeting(""));
    }

    @Test
    public void testGetGreetingAlbanian() {
        GreetingGenerator greetingGenerator = new GreetingGenerator("albanian");

        assertEquals("Përshëndetje Sue", greetingGenerator.GetGreeting("Sue"));

        assertEquals("Përshëndetje ", greetingGenerator.GetGreeting(null));

        assertEquals("Përshëndetje ", greetingGenerator.GetGreeting(""));
    }
}