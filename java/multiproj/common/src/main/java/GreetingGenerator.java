import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GreetingGenerator {

    private static Map<String, String> greetingTemplates = Stream.of(new String[][] {
            { "english", "Hello %s" },
            { "hawaiian", "Aloha %s" },
            { "spanish", "Hola %s" },
            { "german", "Hallo %s" },
            { "albanian", "Përshëndetje %s"}
    }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

    private String language;

    public GreetingGenerator() {
        this(null);
    }

    public GreetingGenerator(String language) {
        if (language==null || language.isEmpty() || !greetingTemplates.containsKey(language.trim().toLowerCase()))
            language="english";

        this.language = language.trim().toLowerCase();
    }

    public String getLanguage() {
        return language;
    }

    public String GetGreeting(String personName)
    {
        if (personName==null)
            personName="";

        return String.format(greetingTemplates.get(language), personName);
    }

}
