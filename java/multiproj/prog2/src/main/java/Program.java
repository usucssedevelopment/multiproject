public class Program {
    public static void main(String[] args) {
        String language="english";
        String name="anyone";

        if (args.length>0)
            language = args[0];

        if (args.length>1)
            name = args[1];

        GreetingGenerator greetingGenerator = new GreetingGenerator(language);

        System.out.println(greetingGenerator.GetGreeting(name));
    }
}
